//*************************************
// Honor Code: The work we are submitting is a result of our own thinking and efforts.
// Cassie Bachik, Sydney Fernandez 
// CMPSC 111 Spring 2017
// Lab 6
// Date: 03-02-2017
//
// Purpose: The purpose of this program is to manipulate a string of DNA in multiple ways. 
//*************************************

import java.util.Date;
import java.util.Scanner; 
import java.util.Random; 

public class ManipulateDNA
{
	//-----------------------------
	// Main method: Program execution begins here
	//-----------------------------

	public static void main(String[] args)
	{ 
	
		// Label output with name and date: 

		System.out.println("Cassie Bachik, Sydney Fernandez\nLab 6\n"+ new Date()+ "\n"); 

		// Variable dictionary: 	
		Scanner scan = new Scanner(System.in); // used for input 
		Random rand = new Random(); // random number generator 
		String dnaString2; // half of the complement of the DNA string
		String dnaString3; // full complement of the DNA string
		int len; // length of the DNA string 
		int location; // one of the positions from the DNA string 
		char c; // a letter randomly chosen from the DNA string 

		System.out.println("Please enter a string of DNA containing only C, G, T, and A: "); 
		String dnaString = scan.next(); // allows user to input any DNA string

		dnaString = dnaString.toUpperCase(); // makes sure DNA string is printed in all uppercase letters

		dnaString2 = dnaString.replace('A','a'); // lowercase so original A's are changed to T complement  
		dnaString2 = dnaString2.replace('T','A'); 
		dnaString2 = dnaString2.replace('a','T'); 
		dnaString3 = dnaString2.replace('C','c'); // lowercase so original C's are changed to G complement
		dnaString3 = dnaString3.replace('G','C'); 
		dnaString3 = dnaString3.replace('c','G'); 
				
		System.out.println("The complement of " + dnaString + " is: " + dnaString3);

		len = dnaString.length(); // calculates length of the DNA string 
		location = rand.nextInt(len+1); // chooses a random position in the DNA string 
		c = dnaString.charAt(rand.nextInt(len)); // chooses a random letter from the DNA string 
		dnaString2 = dnaString.substring(0,location) + c + dnaString.substring(location); 
		// inserts random letter in random position 

		System.out.println("Inserting " + c + " at position " + location + " gives: " + dnaString2); 

		len = dnaString.length(); // calculates length of the DNA string 
		location = rand.nextInt(len); // chooses a random position in the DNA string  
		dnaString2 = dnaString.substring(0,location) + dnaString.substring(location+1); 
		// removes letter from random position by printing everything before and after the location 

		System.out.println("Deleting from position " + location + " gives: " + dnaString2); 

		c = dnaString.charAt(rand.nextInt(len));
		len = dnaString.length(); 
		location = rand.nextInt(len+1); 
		dnaString2 = dnaString.substring(0,location) + c + dnaString.substring(location+1); 

		System.out.println("Changing position " + location + " gives: " + dnaString2); 
		
	}
}

